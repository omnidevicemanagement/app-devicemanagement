package com.bca.app.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.app.model.Borrower;
import com.bca.app.repository.BorrowerRepository;
import com.bca.app.service.BorrowerService;

@Service
public class BorrowerServiceImpl implements BorrowerService{

	@Autowired
	BorrowerRepository borrowerrepository;
	
	@Override
	public List<Borrower> listBorrower() {
		return borrowerrepository.findAll();
	}
	
	@Override
	public Borrower addBorrower(Borrower borrower) {
		return borrowerrepository.save(borrower);
	}
	
	@Override
	public Borrower findBorrower(int id) {
		return borrowerrepository.findById(id).get();
	}
}
