package com.bca.app.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.app.model.Loan;
import com.bca.app.model.LoanUser;
import com.bca.app.repository.LoanRepository;
import com.bca.app.service.LoanService;
import static com.bca.app.model.NamingConstant.*;

@Service
public class LoanServiceImpl implements LoanService{
	
	@Autowired
	LoanRepository loanrepository;
	
	@Autowired
	private DeviceServiceImpl deviceservice;
	
	@Override
	public Loan insertLoan(Loan loan) {
		deviceservice.updateStatusDevice(loan.getDevice().getIdDevice(), ONLOAN);
		return loanrepository.save(loan);
	}
	
	@Override
	public Loan returnLoan(int id) {
		Loan newloan = loanrepository.findById(id).get();
		if(newloan.getLoanID() != 0 ) {
			newloan.setIsReturned(true);
			newloan = loanrepository.save(newloan);
			deviceservice.updateStatusDevice(newloan.getDevice().getIdDevice(), AVAILABLE);
			return newloan;
		}
		else
			return null;		
	}
	
	public List<LoanUser> currentLoanUser(int id_borrower) {
		return loanrepository.currentLoanUser(id_borrower);
	}
	
	public List<LoanUser> historyLoanUser(int id_borrower) {
		return loanrepository.historyLoanUser(id_borrower);
	}
	
	@Override
	public List<Loan> getLoan() {
		return loanrepository.findAll();
	}
	
}
