package com.bca.app.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.app.model.DetailDevice;
import com.bca.app.model.Device;
import com.bca.app.repository.DeviceRepository;
import com.bca.app.service.DeviceService;
import static com.bca.app.model.NamingConstant.AVAILABLE;

@Service
public class DeviceServiceImpl implements DeviceService {
	
	@Autowired
	DeviceRepository deviceRepository;
	
	@Override
	public List<Device> viewAllDevice() {
		return deviceRepository.findAll();
	}
	
	public List<Device> viewDevice() {
		return deviceRepository.findByIsDeleted(false);
	}
	
	public List<Device> availableDevice() {
		return deviceRepository.findByBookStatus(AVAILABLE);
	}
	
	@Override
	public Device addDevice(Device device) {
		device.setBookStatus("Available");
		device.setIsDeleted(false);
		return deviceRepository.save(device);	
	}
	
	@Override
	public Device updateDevice(Device device) {
		Optional<Device> newdevice = deviceRepository.findById(device.getIdDevice());
		if(newdevice.isPresent()) {
			device.setCreatedBy(newdevice.get().getCreatedBy());
		}
		else
			return null;
		return deviceRepository.save(device);	
	}
	
	@Override
	public void deleteDevice(int id) {
		Device deletedevice = deviceRepository.findById(id).get();
		deletedevice.setIsDeleted(true);
		deviceRepository.save(deletedevice);
	}
	
	@Override
	public Device findDevice(int id) {
		return deviceRepository.findById(id).get();
	}
	
	@Override
	public Boolean updateStatusDevice(int id, String status) {
		Device device = deviceRepository.findById(id).get();
		if(device.getIdDevice() != 0) {
			device.setBookStatus(status);
			deviceRepository.save(device);
			return true;
		}
		else
			return false;
	}
	
	public List<DetailDevice> detailDevice(){
		return deviceRepository.detailDevice();
	}
	
}
