package com.bca.app.service;

import java.util.List;

import com.bca.app.model.Loan;

public interface LoanService {
	Loan insertLoan(Loan loan);
	Loan returnLoan(int id);
	List<Loan> getLoan();
}
