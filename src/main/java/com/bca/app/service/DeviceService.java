package com.bca.app.service;

import java.util.List;

import com.bca.app.model.Device;

public interface DeviceService {

	List<Device> viewAllDevice();
	Device addDevice(Device device);
	Device updateDevice(Device device);
	void deleteDevice(int id);
	Device findDevice(int id);
	Boolean updateStatusDevice(int id,String status);
}
