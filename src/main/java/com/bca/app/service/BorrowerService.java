package com.bca.app.service;

import java.util.List;

import com.bca.app.model.Borrower;

public interface BorrowerService {

	List<Borrower> listBorrower();
	Borrower addBorrower(Borrower borrower);
	Borrower findBorrower(int id);
}
