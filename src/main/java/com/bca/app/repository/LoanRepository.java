package com.bca.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bca.app.model.LoanUser;
import com.bca.app.model.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer>{
	
	@Query(value = "select loanID,nama,fromLoan,toLoan from loan l join device d "
			+ "on l.idDevice = d.idDevice where isReturned = false AND l.idBorrower = ?1" , nativeQuery = true)
	List<LoanUser> currentLoanUser(int idBorrower);
	
	@Query(value = "select loanID,nama,fromLoan,toLoan from loan l join device d "
			+ "on l.idDevice = d.idDevice where isReturned = true  AND l.idBorrower = ?1" , nativeQuery = true)
	List<LoanUser> historyLoanUser(int idBorrower);
	
	@Query(value = "update loan SET isReturned = 'true' where idBorrower = ?1 AND idDevice = ?2",nativeQuery = true)
	Loan returnLoan(int idBorrower , int idDevice);
}
