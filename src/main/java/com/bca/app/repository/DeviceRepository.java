package com.bca.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bca.app.model.DetailDevice;
import com.bca.app.model.Device;
import java.lang.String;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {

	List<Device> findByIsDeleted(Boolean isDeleted);
	
	List<Device> findByBookStatus(String bookstatus);
	
	@Query(value = "SELECT * FROM (SELECT idDevice,nama AS 'namaDevice',type,serialNumber,operatingSystem,"
			+ "deviceDescription,bookStatus FROM device)d LEFT JOIN (SELECT loanID,fromLoan,toLoan,idDevice "
			+ "AS 'id_Device',idBorrower from loan where isReturned = 'false')l on d.idDevice = l.id_Device "
			+ "LEFT JOIN (SELECT idBorrower AS 'id_Borrower',nama AS 'namaBorrower' from borrower )b "
			+ "on l.idBorrower = b.id_Borrower" ,nativeQuery = true)
	List<DetailDevice> detailDevice();
}