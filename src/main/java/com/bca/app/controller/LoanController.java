package com.bca.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.app.model.Borrower;
import com.bca.app.model.Device;
import com.bca.app.model.Loan;
import com.bca.app.model.LoanUser;
import com.bca.app.serviceImpl.LoanServiceImpl;

@RestController
@RequestMapping("/loan")
public class LoanController {

	@Autowired
	private LoanServiceImpl loanservice;	

	
	@PostMapping
	public Loan insertloan(@Valid @RequestBody Loan loan) {
		Borrower b = new Borrower();
		Device d = new Device();
		b.setIdBorrower(loan.getBorrowerid());
		d.setIdDevice(loan.getDeviceid());
		loan.setBorrower(b);
		loan.setDevice(d);
		return loanservice.insertLoan(loan);
	}
	
	@GetMapping("/current/{idBorrower}")
	public List<LoanUser> currentloanuser(@PathVariable int idBorrower){
		return loanservice.currentLoanUser(idBorrower);
	}
	
	@GetMapping("/history/{idBorrower}")
	public List<LoanUser> historyloanuser(@PathVariable int idBorrower){
		return loanservice.historyLoanUser(idBorrower);
	}
	
	@GetMapping("/return/{id}")
	public Loan returnloan(@PathVariable int id) {
		return loanservice.returnLoan(id);
	}
	
	@GetMapping
	public List<Loan> getloan(){
		return loanservice.getLoan();
	}
	
}
