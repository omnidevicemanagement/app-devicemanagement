package com.bca.app.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	@GetMapping("/")
	public String hello() {
		return "Welcome to Device Management. In here you can borrow something new device. ^^";
	}
}
