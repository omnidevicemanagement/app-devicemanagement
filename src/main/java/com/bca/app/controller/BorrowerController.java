package com.bca.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.app.model.Borrower;
import com.bca.app.serviceImpl.BorrowerServiceImpl;

@RestController
@RequestMapping("/borrower")
public class BorrowerController {

	@Autowired
	private BorrowerServiceImpl borrowservice;
	
	@GetMapping
	public List<Borrower> listborrower(){
		return borrowservice.listBorrower();
	}
	
	@PostMapping
	public Borrower addborrower(@Valid @RequestBody Borrower borrower) {
		return borrowservice.addBorrower(borrower);
	}
	
	@GetMapping("/{id}")
	public Borrower findborrower(@PathVariable int id) {
		return borrowservice.findBorrower(id);
	}
}
