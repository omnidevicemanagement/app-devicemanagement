package com.bca.app.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.app.model.DetailDevice;
import com.bca.app.model.Device;
import com.bca.app.serviceImpl.DeviceServiceImpl;

@RestController
@RequestMapping("/device")
public class DeviceController {
	
	@Autowired
	private DeviceServiceImpl deviceservice;
	
	@GetMapping
	public List<Device> viewdevice(){
		return deviceservice.viewDevice();
	}
	
	@GetMapping("/available")
	public List<Device> availabledevice(){
		return deviceservice.availableDevice();
	}
	
	@PostMapping
	public Device adddevice(@Valid @RequestBody Device device){
		return deviceservice.addDevice(device);
	}
	
	@PutMapping
	public Device updatedevice(@Valid @RequestBody Device device){
		return deviceservice.updateDevice(device);
	}
	
	@DeleteMapping("/{id}")
	public void deletedevice(@PathVariable int id){
		deviceservice.deleteDevice(id);
	}

	@GetMapping("/{id}")
	public Device finddevice(@PathVariable int id) {
		return deviceservice.findDevice(id);
	}
	
	@GetMapping("/detail")
	public List<DetailDevice> detaildevice(){
		return deviceservice.detailDevice();
	}
}
