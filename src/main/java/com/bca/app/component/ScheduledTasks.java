package com.bca.app.component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bca.app.model.Loan;
import com.bca.app.serviceImpl.DeviceServiceImpl;
import com.bca.app.serviceImpl.LoanServiceImpl;
import static com.bca.app.model.NamingConstant.OVERDUE;

@Component
public class ScheduledTasks {
	@Autowired
	private LoanServiceImpl loanservice;
	
	@Autowired
	private DeviceServiceImpl deviceservice;
	
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Scheduled(cron = "0 15 * * * ?")
    public void scheduleTaskWithFixedRate() {
        logger.info("Fixed Rate Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()) );
        List<Loan> loan =  loanservice.getLoan();
        
        for (Loan l : loan) {
        	if(l.getToLoan().compareTo(new java.util.Date()) == -1) {
        		deviceservice.updateStatusDevice(l.getDevice().getIdDevice(), OVERDUE );
        	}
		}      
    }
}
