package com.bca.app.model;

public class NamingConstant {

	public static final String AVAILABLE = "Available";
	public static final String ONLOAN = "On Loan";
	public static final String OVERDUE = "OverDue";
	
}
