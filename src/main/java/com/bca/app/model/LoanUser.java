package com.bca.app.model;

import java.util.Date;

public interface LoanUser {
	int getLoanID();
	String getNama();
	Date getFromLoan();
	Date getToLoan();
}
