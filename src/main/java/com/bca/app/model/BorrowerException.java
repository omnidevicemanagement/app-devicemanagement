package com.bca.app.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class BorrowerException extends RuntimeException {

  public BorrowerException(String exception) {
    super(exception);
  }

}
