package com.bca.app.model;

import java.util.Date;

public interface DetailDevice {
	int getIdDevice();
	String getNamaDevice();
	String getType();
	String getSerialNumber();
	String getOperatingSystem();
	String getDeviceDescription();
	String getBookStatus();
	String getLoanID();
	Date getFromLoan();
	Date getToLoan();
	String getIdBorrower();
	String getNamaBorrower();
}
