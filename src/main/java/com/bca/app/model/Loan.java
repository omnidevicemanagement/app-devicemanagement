package com.bca.app.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity 
@Table(name = "Loan")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class Loan {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "loanID")
	private int loanID;
	
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "idBorrower", referencedColumnName = "idBorrower" , updatable=false)
    private Borrower borrower;	
    
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "idDevice", referencedColumnName = "idDevice")
	private Device device;
	
    @Transient
    private int borrowerid;
    
    @Transient
    private int deviceid;
    
	@NotNull
	private Date fromLoan;
	
	@NotNull
	private Date toLoan;
	
	@Column(name = "isReturned")
	private boolean isReturned;
	
	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;
    
    @Column(name="isDeleted")
    private boolean isDeleted = false;
    
	@NotNull
    private String createdBy;

	@NotNull
    private String modifiedBy;
	
    @Nullable
	private String userDescription;

	public int getLoanID() {
		return loanID;
	}

	public void setLoanID(int loanID) {
		this.loanID = loanID;
	}

	public Borrower getBorrower() {
		return borrower;
	}

	public void setBorrower(Borrower borrower) {
		this.borrower = borrower;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public int getBorrowerid() {
		return borrowerid;
	}

	public void setBorrowerid(int borrowerid) {
		this.borrowerid = borrowerid;
	}

	public int getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(int deviceid) {
		this.deviceid = deviceid;
	}

	public Date getFromLoan() {
		return fromLoan;
	}

	public void setFromLoan(Date fromLoan) {
		this.fromLoan = fromLoan;
	}

	public Date getToLoan() {
		return toLoan;
	}

	public void setToLoan(Date toLoan) {
		this.toLoan = toLoan;
	}

	public boolean isReturned() {
		return isReturned;
	}

	public void setIsReturned(boolean isReturned) {
		this.isReturned = isReturned;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getUserDescription() {
		return userDescription;
	}

	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}

	@Override
	public String toString() {
		return "Loan [loanID=" + loanID + ", borrower=" + borrower + ", device=" + device + ", fromLoan=" + fromLoan + 
				", toLoan=" + toLoan + ", isReturned=" + isReturned + ", createdAt=" + createdAt + ", updatedAt=" + 
				updatedAt + ", isDeleted=" + isDeleted + ", createdBy=" + createdBy + ", modifiedBy=" + modifiedBy + 
				", userDescription=" + userDescription + "]";
	}
}
