package com.bca.app.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bca.app.model.DetailDevice;
import com.bca.app.model.Device;
import com.bca.app.repository.DeviceRepository;
import com.bca.app.serviceImpl.DeviceServiceImpl;
import static com.bca.app.model.NamingConstant.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DeviceServiceTest {
	
	Device mockDevice;
	List<Device> mockDeviceList;
	List<DetailDevice> mockDetailDeviceList;
	
	@MockBean
	DeviceRepository mDeviceRepository;
	
	@InjectMocks
	DeviceServiceImpl service;
	
	@Before
	public void setup() throws Exception{
		MockitoAnnotations.initMocks(this);
		
		mockDevice = new Device();
		mockDeviceList = new ArrayList<>();
		mockDetailDeviceList = new ArrayList<>();
	}
	
	@Test
	public void shouldAddDevice() throws Exception{
		when(mDeviceRepository.save(any(Device.class)))
			.thenReturn(mockDevice);
		
		assertThat(service.addDevice(mockDevice), is(notNullValue()));
		
		verify(mDeviceRepository, times(1))
			.save(any(Device.class));
		
		verifyNoMoreInteractions(mDeviceRepository);
		
	}
	
	@Test
	public void shouldNotUpdateDevice() throws Exception{
		//should not update device if device not found
		
		//new device is not present
		when(mDeviceRepository.findById(anyInt()))
			.thenReturn(Optional.empty());
		
		service.updateDevice(mockDevice);
		
		verify(mDeviceRepository, times(1))
			.findById(anyInt());
		
		verify(mDeviceRepository, never())
			.save(any(Device.class));
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
	
	@Test
	public void shouldUpdateDevice() throws Exception{
		//should update device if only device is found
		
		//new device is present
		when(mDeviceRepository.findById(anyInt()))
			.thenReturn(Optional.of(mockDevice));
		
		service.updateDevice(mockDevice);
		
		verify(mDeviceRepository, times(1))
			.findById(anyInt());
		
		verify(mDeviceRepository, times(1))
			.save(any(Device.class));
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
	
	@Test
	public void shouldDeleteDevice() throws Exception{
		when(mDeviceRepository.findById(anyInt()))
			.thenReturn(Optional.of(mockDevice));
		
		service.deleteDevice(anyInt());

		verify(mDeviceRepository)
			.findById(anyInt());
		
		verify(mDeviceRepository)
			.save(any(Device.class));
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
	
	@Test
	public void shouldViewAllDevice() throws Exception{
		when(mDeviceRepository.findAll())
			.thenReturn(mockDeviceList);
		
		assertThat(service.viewAllDevice(), is(notNullValue()));
		
		verify(mDeviceRepository)
			.findAll();
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
	
	@Test
	public void shouldViewDevice() throws Exception{
		when(mDeviceRepository.findByIsDeleted(false))
			.thenReturn(mockDeviceList);
		
		assertThat(service.viewDevice(), is(notNullValue()));
		
		verify(mDeviceRepository)
			.findByIsDeleted(false);
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
	
	@Test
	public void shouldViewAllAvailableDevice() throws Exception{
		when(mDeviceRepository.findByBookStatus(AVAILABLE))
			.thenReturn(mockDeviceList);
		
		assertThat(service.availableDevice(), is(notNullValue()));
		
		verify(mDeviceRepository)
			.findByBookStatus(AVAILABLE);
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
	
	@Test
	public void shouldFindDevice() throws Exception{
		when(mDeviceRepository.findById(anyInt()))
			.thenReturn(Optional.of(mockDevice));
		
		assertThat(service.findDevice(anyInt()), is(notNullValue()));
		
		verify(mDeviceRepository)
			.findById(anyInt());
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
	
	@Test
	public void shouldDetailDevice() throws Exception{
		when(mDeviceRepository.detailDevice())
			.thenReturn(mockDetailDeviceList);
		
		assertThat(service.detailDevice(), is(notNullValue()));
		
		verify(mDeviceRepository)
			.detailDevice();
		
		verifyNoMoreInteractions(mDeviceRepository);
	}
}
