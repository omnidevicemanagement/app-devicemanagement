package com.bca.app.service;

import static org.mockito.ArgumentMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bca.app.model.Borrower;
import com.bca.app.repository.BorrowerRepository;
import com.bca.app.serviceImpl.BorrowerServiceImpl;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BorrowerServiceTest {

	Borrower mockBorrower;
	List<Borrower> mockBorrowerList;
	
	@MockBean
	BorrowerRepository mBorrowerRepository;
	
	@InjectMocks
	BorrowerServiceImpl service;
	
	@Before
	public void setup() throws Exception{
		MockitoAnnotations.initMocks(this);
		
		mockBorrower = new Borrower();
		mockBorrowerList = new ArrayList<>();
	}
	
	@Test
	public void shouldListBorrower() {
		when(mBorrowerRepository.findAll())
			.thenReturn(mockBorrowerList);
		
		assertThat(service.listBorrower(), is(notNullValue()));
		
		verify(mBorrowerRepository)
			.findAll();
		
		verifyNoMoreInteractions(mBorrowerRepository);
	}
	
	@Test
	public void shouldAddBorrower() {
		when(mBorrowerRepository.save(any(Borrower.class)))
			.thenReturn(mockBorrower);
		
		assertThat(service.addBorrower(mockBorrower), is(notNullValue()));
		
		verify(mBorrowerRepository, times(1))
			.save(any(Borrower.class));
		
		verifyNoMoreInteractions(mBorrowerRepository);
	}
	
	@Test
	public void shouldFindBorrower() {
		when(mBorrowerRepository.findById(anyInt()))
			.thenReturn(Optional.of(mockBorrower));
		
		assertThat(service.findBorrower(anyInt()), is(notNullValue()));
		
		verify(mBorrowerRepository)
			.findById(anyInt());
		
		verifyNoMoreInteractions(mBorrowerRepository);
	}
}
