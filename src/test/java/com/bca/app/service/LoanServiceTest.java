package com.bca.app.service;

import static org.mockito.ArgumentMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bca.app.model.Device;
import com.bca.app.model.Loan;
import com.bca.app.model.LoanUser;
import com.bca.app.repository.LoanRepository;
import com.bca.app.serviceImpl.DeviceServiceImpl;
import com.bca.app.serviceImpl.LoanServiceImpl;
import static com.bca.app.model.NamingConstant.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LoanServiceTest {
	
	Loan mockLoan;
	Device mockDevice;
	List<LoanUser> mockLoanList;
	List<Loan> mockJustLoanList;
	
	@MockBean
	LoanRepository mLoanRepository;
	
	@MockBean
	DeviceServiceImpl mDeviceService;
	
	@InjectMocks
	LoanServiceImpl service;
	
	@Before
	public void setup() throws Exception{
		MockitoAnnotations.initMocks(this);

		mockLoan = new Loan();
		mockDevice = new Device();
		mockLoanList = new ArrayList<>();
		mockJustLoanList = new ArrayList<>();
	}
	
	@Test
	public void shouldInsertLoan() {
		final int ID_DEVICE_DUMMY = 1;
		
		when(mLoanRepository.save(any(Loan.class)))
			.thenReturn(mockLoan);
		
		mockDevice.setIdDevice(ID_DEVICE_DUMMY);
		mockLoan.setDevice(mockDevice);
		
		when(mDeviceService.updateStatusDevice(eq(ID_DEVICE_DUMMY), eq(ONLOAN)))
			.thenReturn(true);
		
		assertTrue(mDeviceService.updateStatusDevice(ID_DEVICE_DUMMY, ONLOAN));
		assertThat(service.insertLoan(mockLoan), is(notNullValue()));
		
		verify(mLoanRepository, times(1))
			.save(any(Loan.class));
		
		verifyNoMoreInteractions(mLoanRepository);
	}
	
	@Test
	public void shouldNotReturnLoan() {
		//should not return device if loan id == 0
		
		mockLoan.setLoanID(0);
		
		when(mLoanRepository.findById(anyInt()))
			.thenReturn(Optional.of(mockLoan));
		
		service.returnLoan(anyInt());
		
		verify(mLoanRepository, times(1))
			.findById(anyInt());
		
		verify(mLoanRepository, never())
			.save(any(Loan.class));
		
		verifyNoMoreInteractions(mLoanRepository);
				
	}

	
	@Test
	public void shouldReturnLoan() {
		//should return device if loan id != 0
		mockLoan.setLoanID(1);
		
		when(mLoanRepository.findById(anyInt()))
			.thenReturn(Optional.of(mockLoan));

		//to assert that device status become 'available'
		final int ID_DEVICE_DUMMY = 1;
		mockDevice.setIdDevice(ID_DEVICE_DUMMY);
		mockLoan.setDevice(mockDevice);
		when(mDeviceService.updateStatusDevice(eq(ID_DEVICE_DUMMY), eq(AVAILABLE)))
			.thenReturn(true);
		when(mLoanRepository.save(any(Loan.class)))
			.thenReturn(mockLoan);
		assertTrue(mDeviceService.updateStatusDevice(ID_DEVICE_DUMMY, AVAILABLE));
		
		service.returnLoan(anyInt());

		verify(mDeviceService, times(2)) //1 for assertion, 1 for real implementation
			.updateStatusDevice(eq(ID_DEVICE_DUMMY), eq(AVAILABLE));
		
		verify(mLoanRepository, times(1))
			.findById(anyInt());
		
		verify(mLoanRepository, times(1))
			.save(any(Loan.class));
		
		verifyNoMoreInteractions(mLoanRepository);
		
		verifyNoMoreInteractions(mDeviceService);
	}
	
	
	@Test
	public void shouldListCurrentLoanUser() {
		when(mLoanRepository.currentLoanUser(anyInt()))
			.thenReturn(mockLoanList);
		
		assertThat(service.currentLoanUser(anyInt()), is(notNullValue()));
	
		verify(mLoanRepository)
			.currentLoanUser(anyInt());
		
		verifyNoMoreInteractions(mLoanRepository);
	}
	
	@Test
	public void shouldListHistoryLoanUser() {
		when(mLoanRepository.historyLoanUser(anyInt()))
			.thenReturn(mockLoanList);
		
		assertThat(service.historyLoanUser(anyInt()), is(notNullValue()));
	
		verify(mLoanRepository)
			.historyLoanUser(anyInt());
		
		verifyNoMoreInteractions(mLoanRepository);
	}
	
	@Test
	public void shouldGetLoan() {
		when(mLoanRepository.findAll())
			.thenReturn(mockJustLoanList);
		
		assertThat(service.getLoan(), is(notNullValue()));
		
		verify(mLoanRepository)
			.findAll();
		
		verifyNoMoreInteractions(mLoanRepository);
	}
}
