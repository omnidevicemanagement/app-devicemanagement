package com.bca.app;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bca.app.controller.DeviceController;
import com.bca.app.model.MockDevice;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

@SpringBootTest(classes = {MockDevice.class})
@RunWith(SpringRunner.class)
public class DeviceIT {

	final static String SAMPLE_ID = "1";
	final static String SAMPLE_NAMA = "Maria";
	final static String SAMPLE_TYPE = "Samsung Galaxy A8";
	final static String SAMPLE_SERIAL_NUMBER = "SN6734";
	final static String SAMPLE_OS = "Android";
	final static String SAMPLE_DEVICE_DESCRIPTION = "My First Phone";
	final static String SAMPLE_CREATED_BY = "James";
	final static String SAMPLE_MODIFIED_BY = "Martha";
	final static String SAMPLE_USER_DESCRIPITOIN = "About user";
	
	MockMvc mockMvc;
	
	JsonObject requestBody;
	
	@Autowired
	DeviceController mDeviceController;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(mDeviceController).build();
		
		requestBody = new JsonObject();
		requestBody.addProperty("nama", SAMPLE_NAMA);
		requestBody.addProperty("type", SAMPLE_TYPE);
		requestBody.addProperty("serialNumber", SAMPLE_SERIAL_NUMBER);
		requestBody.addProperty("operatingSystem", SAMPLE_OS);
		requestBody.addProperty("deviceDescription", SAMPLE_DEVICE_DESCRIPTION);
		requestBody.addProperty("createdBy", SAMPLE_CREATED_BY);
		requestBody.addProperty("modifiedBy", SAMPLE_MODIFIED_BY);
	}
	
	@Test
	public void shouldAddDevice() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/device")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("type").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("serialNumber").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("operatingSystem").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("bookStatus").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("isDeleted").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("idDevice").exists());
	}
	
	@Test
	public void shouldViewAvailableDevice() throws Exception {
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/device/available"))
			.andExpect(status().isOk())
			
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].type").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].serialNumber").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].operatingSystem").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdBy").exists())
			
			.andReturn();
		
		System.out.println("Response available device: " + mvcResult.getResponse().getContentAsString());
	}

	
	@Test
	public void shouldViewDevice() throws Exception{
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/device"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].type").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].serialNumber").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].operatingSystem").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].deviceDescription").exists())
			//.andExpect(MockMvcResultMatchers.jsonPath("$.[0].bookStatus").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].isDeleted").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdBy").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].idDevice").exists())
			.andReturn();
		
		System.out.println("Response view: " + mvcResult.getResponse().getContentAsString());
	}
	
	@Test
	public void shouldUpdateDevice() throws Exception{
		requestBody.addProperty("idDevice", SAMPLE_ID);
		requestBody.addProperty("userDescription", SAMPLE_USER_DESCRIPITOIN);
		
		mockMvc.perform(MockMvcRequestBuilders.put("/device")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("type").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("serialNumber").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("operatingSystem").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("isDeleted").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("userDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("idDevice").exists());
	}
	
	@Test
	public void shouldShowViewDeviceById() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/device/1"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("type").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("serialNumber").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("operatingSystem").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").exists());
	}
	
	@Test
	public void shouldDeleteDevice() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.delete("/device/1"))
			.andExpect(status().isOk())
			.andReturn();
		
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/device/1"))
			.andReturn();
				
		JsonObject responseBody = new GsonBuilder()
				.create()
				.fromJson(mvcResult.getResponse().getContentAsString(), JsonObject.class);
		
		assertTrue("true".equals(""+responseBody.get("isDeleted")));
	}

}
