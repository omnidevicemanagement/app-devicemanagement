package com.bca.app.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bca.app.controller.DeviceController;
import com.bca.app.model.DetailDevice;
import com.bca.app.model.Device;
import com.bca.app.serviceImpl.DeviceServiceImpl;
import com.google.gson.JsonObject;

@SpringBootTest
@RunWith(SpringRunner.class)//
public class DeviceControllerTest {

	final String SAMPLE_NAMA = "Maria";
	final String SAMPLE_TYPE = "Samsung Galaxy A8";
	final String SAMPLE_SERIAL_NUMBER = "SN6734";
	final String SAMPLE_OS = "Android";
	final String SAMPLE_DEVICE_DESCRIPTION = "";
	final String SAMPLE_CREATED_BY = "James";
	final String SAMPLE_MODIFIED_BY = "Martha";
	final String SAMPLE_CREATED_AT= "2018-10-07T12:22:40.000+0000";
	final String SAMPLE_UPDATED_AT = "2018-10-07T12:22:40.000+0000";
	final boolean SAMPLE_IS_DELETED = false;
	final String SAMPLE_USER_DESCRIPTION = "description";
	
    final int SAMPLE_DETAIL_ID_DEVICE = 1;
	final String SAMPLE_DETAIL_NAMA_DEVICE = "Maria";
	final String SAMPLE_DETAIL_TYPE = "Samsung Galaxy A8";
	final String SAMPLE_DETAIL_SERIAL_NUMBER = "SN6734";
	final String SAMPLE_DETAIL_OPERATING_SYSTEM = "Android";
	final String SAMPLE_DETAIL_DEVICE_DESCRIPTION = "My first phone";
	final String SAMPLE_DETAIL_BOOK_STATUS = "Available";
	final String SAMPLE_DETAIL_LOAN_ID = "0";
	final java.sql.Date SAMPLE_DETAIL_FROM_LOAN = new java.sql.Date(System.currentTimeMillis());
	final java.sql.Date SAMPLE_DETAIL_TO_LOAN = new java.sql.Date(System.currentTimeMillis());
	final String SAMPLE_DETAIL_ID_BORROWER = "0";
	final String SAMPLE_DETAIL_NAMA_BORROWER = "My child";
	
	MockMvc mockMvc;
	
	//Class to be tested
	@Autowired
	DeviceController controller;
	
	//Dependencies (will be mocked)
	@MockBean
	DeviceServiceImpl mDeviceService;
	
	//Test Data
	Device sampleDevice;
	DetailDevice sampleDetailDevice;
	List<Device> sampleDeviceList;
	List<DetailDevice> sampleDetailDeviceList;
	JsonObject requestBody;
	
	@Before
	public void setup() throws Exception{
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
		
		sampleDevice = new Device();
		sampleDevice.setNama(SAMPLE_NAMA);
		sampleDevice.setType(SAMPLE_TYPE);
		sampleDevice.setSerialNumber(SAMPLE_SERIAL_NUMBER);
		sampleDevice.setOperatingSystem(SAMPLE_OS);
		sampleDevice.setDeviceDescription(SAMPLE_DEVICE_DESCRIPTION);
		sampleDevice.setCreatedBy(SAMPLE_CREATED_BY);
		sampleDevice.setModifiedBy(SAMPLE_MODIFIED_BY);
		sampleDevice.setCreatedAt(new Date());
		sampleDevice.setUpdatedAt(new Date());
		sampleDevice.setDeviceDescription(SAMPLE_USER_DESCRIPTION);

		sampleDetailDevice = new DetailDevice() {
			
			@Override
			public String getType() {
				return SAMPLE_DETAIL_TYPE;
			}
			
			@Override
			public java.sql.Date getToLoan() {
				return SAMPLE_DETAIL_TO_LOAN;
			}
			
			@Override
			public String getSerialNumber() {
				return SAMPLE_DETAIL_SERIAL_NUMBER;
			}
			
			@Override
			public String getOperatingSystem() {
				return SAMPLE_DETAIL_OPERATING_SYSTEM;
			}
			
			@Override
			public String getNamaDevice() {
				return SAMPLE_DETAIL_NAMA_DEVICE;
			}
			
			@Override
			public String getNamaBorrower() {
				return SAMPLE_DETAIL_NAMA_BORROWER;
			}
			
			@Override
			public String getLoanID() {
				return SAMPLE_DETAIL_LOAN_ID;
			}
			
			@Override
			public int getIdDevice() {
				return SAMPLE_DETAIL_ID_DEVICE;
			}
			
			@Override
			public String getIdBorrower() {
				return SAMPLE_DETAIL_ID_BORROWER;
			}
			
			@Override
			public java.sql.Date getFromLoan() {
				return SAMPLE_DETAIL_FROM_LOAN;
			}
			
			@Override
			public String getDeviceDescription() {
				return SAMPLE_DETAIL_DEVICE_DESCRIPTION;
			}
			
			@Override
			public String getBookStatus() {
				return SAMPLE_DETAIL_BOOK_STATUS;
			}
		};
		
		requestBody = new JsonObject();
		requestBody.addProperty("nama", SAMPLE_NAMA);
		requestBody.addProperty("type", SAMPLE_TYPE);
		requestBody.addProperty("serialNumber", SAMPLE_SERIAL_NUMBER);
		requestBody.addProperty("operatingSystem", SAMPLE_OS);
		requestBody.addProperty("deviceDescription", SAMPLE_DEVICE_DESCRIPTION);
		requestBody.addProperty("createdBy", SAMPLE_CREATED_BY);
		requestBody.addProperty("modifiedBy", SAMPLE_MODIFIED_BY);
		requestBody.addProperty("createdAt", SAMPLE_CREATED_AT);
		requestBody.addProperty("updatedAt", SAMPLE_UPDATED_AT);
		requestBody.addProperty("isDeleted", SAMPLE_IS_DELETED);
		requestBody.addProperty("userDescription", SAMPLE_USER_DESCRIPTION);
	}
	
	@Test
	public void shouldAddDevice() throws Exception {
		when(mDeviceService.addDevice(any(Device.class)))
			.thenReturn(sampleDevice);
		
		System.out.println("Request body: " + requestBody.toString());
		
		mockMvc.perform(MockMvcRequestBuilders.post("/device")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("type").value(SAMPLE_TYPE))
			.andExpect(MockMvcResultMatchers.jsonPath("serialNumber").value(SAMPLE_SERIAL_NUMBER))
			.andExpect(MockMvcResultMatchers.jsonPath("operatingSystem").value(SAMPLE_OS))
			.andExpect(MockMvcResultMatchers.jsonPath("deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("modifiedBy").value(SAMPLE_MODIFIED_BY));
		
		verify(mDeviceService).addDevice(any(Device.class));
		verifyNoMoreInteractions(mDeviceService);
	}
	
	@Test
	public void shouldViewNoDevice() throws Exception{
		when(mDeviceService.viewDevice())
			.thenReturn(Collections.emptyList());
				
		mockMvc.perform(MockMvcRequestBuilders.get("/device"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.content().string("[]"));	
		
		verify(mDeviceService).viewDevice();
		verifyNoMoreInteractions(mDeviceService);
	}
	
	@Test
	public void shouldViewAllDevice() throws Exception{
		sampleDeviceList = new ArrayList<>();
		for(int i = 0; i<5; i++)
			sampleDeviceList.add(sampleDevice);
		
		when(mDeviceService.viewDevice())
			.thenReturn(sampleDeviceList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/device"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].type").value(SAMPLE_TYPE))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].serialNumber").value(SAMPLE_SERIAL_NUMBER))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].operatingSystem").value(SAMPLE_OS))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].deviceDescription").value(SAMPLE_USER_DESCRIPTION))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].modifiedBy").value(SAMPLE_MODIFIED_BY))
			.andReturn();
		
		verify(mDeviceService).viewDevice();
		verifyNoMoreInteractions(mDeviceService);
	}
	
	@Test
	public void shouldUpdateDevice() throws Exception{
		when(mDeviceService.updateDevice(any(Device.class)))
			.thenReturn(sampleDevice);
		
		mockMvc.perform(MockMvcRequestBuilders.put("/device")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("type").value(SAMPLE_TYPE))
			.andExpect(MockMvcResultMatchers.jsonPath("serialNumber").value(SAMPLE_SERIAL_NUMBER))
			.andExpect(MockMvcResultMatchers.jsonPath("operatingSystem").value(SAMPLE_OS))
			.andExpect(MockMvcResultMatchers.jsonPath("deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("modifiedBy").value(SAMPLE_MODIFIED_BY));
		
		verify(mDeviceService).updateDevice(any(Device.class));
		verifyNoMoreInteractions(mDeviceService);
	}
	
	@Test
	public void shouldDeleteDevice() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.delete("/device/" + anyInt()))
			.andExpect(status().isOk());
		
		verify(mDeviceService, times(1)).deleteDevice(anyInt());
		verifyNoMoreInteractions(mDeviceService);
	}
	
	@Test
	public void shouldShowAvailableDevice() throws Exception{
		sampleDeviceList = new ArrayList<>();
		for(int i = 0; i<5; i++)
			sampleDeviceList.add(sampleDevice);
		
		when(mDeviceService.availableDevice())
			.thenReturn(sampleDeviceList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/device/available"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].type").value(SAMPLE_TYPE))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].serialNumber").value(SAMPLE_SERIAL_NUMBER))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].operatingSystem").value(SAMPLE_OS))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].modifiedBy").value(SAMPLE_MODIFIED_BY));
		
		verify(mDeviceService).availableDevice();
		verifyNoMoreInteractions(mDeviceService);
	}
	
	@Test
	public void shouldShowDeviceById() throws Exception {
		when(mDeviceService.findDevice(anyInt()))
			.thenReturn(sampleDevice);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/device/" + anyInt()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("type").value(SAMPLE_TYPE))
			.andExpect(MockMvcResultMatchers.jsonPath("serialNumber").value(SAMPLE_SERIAL_NUMBER))
			.andExpect(MockMvcResultMatchers.jsonPath("operatingSystem").value(SAMPLE_OS))
			.andExpect(MockMvcResultMatchers.jsonPath("deviceDescription").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("modifiedBy").value(SAMPLE_MODIFIED_BY));
		
		verify(mDeviceService).findDevice(anyInt());
		verifyNoMoreInteractions(mDeviceService);
	}
	
	@Test
	public void shouldDetailDevice() throws Exception{
		sampleDetailDeviceList = new ArrayList<>();
		for(int i = 0; i<5; i++)
			sampleDetailDeviceList.add(sampleDetailDevice);
		
		when(mDeviceService.detailDevice())
			.thenReturn(sampleDetailDeviceList);
		
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/device/detail"))
			.andExpect(status().isOk())
			
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].type").value(SAMPLE_DETAIL_TYPE))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].loanID").value(SAMPLE_DETAIL_LOAN_ID))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].bookStatus").value(SAMPLE_DETAIL_BOOK_STATUS))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].idDevice").value(SAMPLE_DETAIL_ID_DEVICE))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].toLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].fromLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].namaBorrower").value(SAMPLE_DETAIL_NAMA_BORROWER))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].serialNumber").value(SAMPLE_DETAIL_SERIAL_NUMBER))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].namaDevice").value(SAMPLE_DETAIL_NAMA_DEVICE))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].idBorrower").value(SAMPLE_DETAIL_ID_BORROWER))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].operatingSystem").value(SAMPLE_DETAIL_OPERATING_SYSTEM))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].deviceDescription").value(SAMPLE_DETAIL_DEVICE_DESCRIPTION))
			
			.andReturn();
		
		System.out.println("Detail Device Response: " + mvcResult.getResponse().getContentAsString());
		
		verify(mDeviceService).detailDevice();
		verifyNoMoreInteractions(mDeviceService);
	}
}
