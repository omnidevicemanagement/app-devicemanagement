package com.bca.app.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bca.app.model.Borrower;
import com.bca.app.serviceImpl.BorrowerServiceImpl;
import com.google.gson.JsonObject;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BorrowerControllerTest {

	final int SAMPLE_ID_BORROWER = 1;
	final String SAMPLE_NIP = "12345";
	final String SAMPLE_NAMA = "Gerald";
	final String SAMPLE_TIM = "Customer Touch Point";
	final Date SAMPLE_CREATED_AT = new Date();
	final Date SAMPLE_UPDATED_AT = new Date();
	final boolean SAMPLE_IS_DELETED = false;
	final String SAMPLE_CREATED_BY = "Me";
	final String SAMPLE_MODIFIED_BY = "You";
	final String SAMPLE_USER_DESCRIPTION = "Tested on October 2018";
	
	MockMvc mockMvc;
	
	@Autowired
	BorrowerController controller;
	
	@MockBean
	BorrowerServiceImpl mBorrowerService;
	
	Borrower sampleBorrower;
	List<Borrower> sampleBorrowerList;
	JsonObject requestBody;
	
	@Before
	public void setup() throws Exception{
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	
		sampleBorrower = new Borrower();
		sampleBorrower.setIdBorrower(SAMPLE_ID_BORROWER);
		sampleBorrower.setNip(SAMPLE_NIP);
		sampleBorrower.setNama(SAMPLE_NAMA);
		sampleBorrower.setTim(SAMPLE_TIM);
		sampleBorrower.setCreatedAt(SAMPLE_CREATED_AT);
		sampleBorrower.setUpdatedAt(SAMPLE_UPDATED_AT);
		sampleBorrower.setIsDeleted(SAMPLE_IS_DELETED);
		sampleBorrower.setCreatedBy(SAMPLE_CREATED_BY);
		sampleBorrower.setModifiedBy(SAMPLE_MODIFIED_BY);
		sampleBorrower.setUserDescription(SAMPLE_USER_DESCRIPTION);
		
		sampleBorrowerList = new ArrayList<>();
		requestBody = new JsonObject();
	}
	
	@Test
	public void shouldListBorrower() throws Exception {	
		for(int i = 0; i<5; i++)
			sampleBorrowerList.add(sampleBorrower);
		
		when(mBorrowerService.listBorrower())
			.thenReturn(sampleBorrowerList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/borrower"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].idBorrower").value(SAMPLE_ID_BORROWER))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nip").value(SAMPLE_NIP))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].tim").value(SAMPLE_TIM))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdAt").value(SAMPLE_CREATED_AT))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].updatedAt").value(SAMPLE_UPDATED_AT))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].isDeleted").value(SAMPLE_IS_DELETED))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].modifiedBy").value(SAMPLE_MODIFIED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].userDescription").value(SAMPLE_USER_DESCRIPTION));
		verify(mBorrowerService).listBorrower();
		verifyNoMoreInteractions(mBorrowerService);
	}
	
	@Test
	public void shouldAddBorrower() throws Exception{
		when(mBorrowerService.addBorrower(any(Borrower.class)))
			.thenReturn(sampleBorrower);
		
		requestBody.addProperty("nip", SAMPLE_NIP);
		requestBody.addProperty("nama", SAMPLE_NAMA);
		requestBody.addProperty("tim", SAMPLE_TIM);
		requestBody.addProperty("createdBy", SAMPLE_CREATED_BY);
		requestBody.addProperty("modifiedBy", SAMPLE_MODIFIED_BY);
		
		System.out.println("Request body: " + requestBody.toString());
		
		mockMvc.perform(MockMvcRequestBuilders.post("/borrower")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("idBorrower").value(SAMPLE_ID_BORROWER))
			.andExpect(MockMvcResultMatchers.jsonPath("nip").value(SAMPLE_NIP))
			.andExpect(MockMvcResultMatchers.jsonPath("nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("tim").value(SAMPLE_TIM))
			.andExpect(MockMvcResultMatchers.jsonPath("createdAt").value(SAMPLE_CREATED_AT))
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").value(SAMPLE_UPDATED_AT))
			.andExpect(MockMvcResultMatchers.jsonPath("isDeleted").value(SAMPLE_IS_DELETED))
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("modifiedBy").value(SAMPLE_MODIFIED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("userDescription").value(SAMPLE_USER_DESCRIPTION));
		
		verify(mBorrowerService).addBorrower(any(Borrower.class));
		verifyNoMoreInteractions(mBorrowerService);
	}
	
	@Test
	public void shouldFindBorrower() throws Exception{
		when(mBorrowerService.findBorrower(anyInt()))
			.thenReturn(sampleBorrower);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/borrower/" + anyInt()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("idBorrower").value(SAMPLE_ID_BORROWER))
			.andExpect(MockMvcResultMatchers.jsonPath("nip").value(SAMPLE_NIP))
			.andExpect(MockMvcResultMatchers.jsonPath("nama").value(SAMPLE_NAMA))
			.andExpect(MockMvcResultMatchers.jsonPath("tim").value(SAMPLE_TIM))
			.andExpect(MockMvcResultMatchers.jsonPath("createdAt").value(SAMPLE_CREATED_AT))
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").value(SAMPLE_UPDATED_AT))
			.andExpect(MockMvcResultMatchers.jsonPath("isDeleted").value(SAMPLE_IS_DELETED))
			.andExpect(MockMvcResultMatchers.jsonPath("createdBy").value(SAMPLE_CREATED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("modifiedBy").value(SAMPLE_MODIFIED_BY))
			.andExpect(MockMvcResultMatchers.jsonPath("userDescription").value(SAMPLE_USER_DESCRIPTION));

		verify(mBorrowerService).findBorrower(anyInt());
		verifyNoMoreInteractions(mBorrowerService);
		
	}
}
