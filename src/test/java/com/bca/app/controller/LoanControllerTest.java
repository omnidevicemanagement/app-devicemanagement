package com.bca.app.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bca.app.model.Borrower;
import com.bca.app.model.Device;
import com.bca.app.model.Loan;
import com.bca.app.model.LoanUser;
import com.bca.app.serviceImpl.BorrowerServiceImpl;
import com.bca.app.serviceImpl.DeviceServiceImpl;
import com.bca.app.serviceImpl.LoanServiceImpl;
import com.google.gson.JsonObject;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LoanControllerTest {

	final Date SAMPLE_LOAN_USER_TO_LOAN = null;
	final Date SAMPLE_LOAN_USER_FROM_LOAN = new Date(System.currentTimeMillis());
	final int SAMPLE_LOAN_USER_LOAN_ID = 1;
	final String SAMPLE_LOAN_USER_NAMA = "iPhone X";
	
	final String SAMPLE_LOAN_BORROWER_ID = "1";
	final String SAMPLE_LOAN_DEVICE_ID = "1";
	final Date SAMPLE_LOAN_FROM_LOAN = new Date(System.currentTimeMillis()-100000);
	final Date SAMPLE_LOAN_TO_LOAN = new Date(System.currentTimeMillis()+100000);
	final String SAMPLE_LOAN_CREATED_BY = "Me";
	final String SAMPLE_LOAN_MODIFIED_BY = "You";
	final String SAMPLE_LOAN_USER_DESCRIPTION = "Nothing do describe";
			
	MockMvc mockMvc;
	
	@Autowired
	LoanController controller;
	
	@MockBean
	LoanServiceImpl mLoanService;
	
	@MockBean
	BorrowerServiceImpl mBorrowerService;
	
	@MockBean
	DeviceServiceImpl mDeviceService;
	
	Loan sampleLoan;
	LoanUser sampleLoanUser;
	Device sampleDevice;
	Borrower sampleBorrower;
	List<LoanUser> sampleLoanUserList;
	List<Loan> sampleLoanList;
	JsonObject requestBody;
	JsonObject borrowerRequestBody;
	JsonObject deviceRequestBody;
	
	@Before
	public void setup() throws Exception{
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	
		sampleLoan = new Loan();
	
		borrowerRequestBody = new JsonObject();
		borrowerRequestBody.addProperty("idBorrower",SAMPLE_LOAN_BORROWER_ID);
		
		deviceRequestBody = new JsonObject();
		deviceRequestBody.addProperty("idDevice",SAMPLE_LOAN_DEVICE_ID);
		
		requestBody = new JsonObject();
		requestBody.add("borrower", borrowerRequestBody);
		requestBody.add("device", deviceRequestBody);
		requestBody.addProperty("fromLoan", ""+SAMPLE_LOAN_FROM_LOAN);
		requestBody.addProperty("toLoan", ""+SAMPLE_LOAN_TO_LOAN);
		requestBody.addProperty("createdBy", SAMPLE_LOAN_CREATED_BY);
		requestBody.addProperty("modifiedBy", SAMPLE_LOAN_MODIFIED_BY);
		requestBody.addProperty("userDescription", SAMPLE_LOAN_USER_DESCRIPTION);
		
		sampleLoanUser = new LoanUser() {
			
			@Override
			public Date getToLoan() {
				return SAMPLE_LOAN_USER_TO_LOAN;
			}
			
			@Override
			public String getNama() {
				return SAMPLE_LOAN_USER_NAMA;
			}
			
			@Override
			public int getLoanID() {
				return SAMPLE_LOAN_USER_LOAN_ID;
			}
			
			@Override
			public Date getFromLoan() {
				return SAMPLE_LOAN_USER_FROM_LOAN;
			}
		};
	}
	
	@Test
	public void shouldInsertLoan() throws Exception{		
		when(mLoanService.insertLoan(any(Loan.class)))
			.thenReturn(sampleLoan);
		
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/loan")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andReturn();
		System.out.println("Insertion response: " + mvcResult.getResponse().getContentAsString());
		
		verify(mLoanService).insertLoan(any(Loan.class));
		verifyNoMoreInteractions(mLoanService);
	}
	
	@Test
	public void shouldGetCurrentLoanUser() throws Exception{
		sampleLoanUserList = new ArrayList<>();
		for(int i = 0; i<5; i++)
			sampleLoanUserList.add(sampleLoanUser);
		
		when(mLoanService.currentLoanUser(anyInt()))
			.thenReturn(sampleLoanUserList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/loan/current/" + anyInt()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].loanID").value(SAMPLE_LOAN_USER_LOAN_ID))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].fromLoan").exists());
		
		verify(mLoanService).currentLoanUser(anyInt());
		verifyNoMoreInteractions(mLoanService);
	}
	
	@Test
	public void shouldGetHistoryLoanUser() throws Exception{
		sampleLoanUserList = new ArrayList<>();
		for(int i = 0; i<5; i++)
			sampleLoanUserList.add(sampleLoanUser);
		
		when(mLoanService.historyLoanUser(anyInt()))
			.thenReturn(sampleLoanUserList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/loan/history/" + anyInt()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].loanID").value(SAMPLE_LOAN_USER_LOAN_ID));
		
		verify(mLoanService).historyLoanUser(anyInt());
		verifyNoMoreInteractions(mLoanService);
	}
	
	@Test
	public void shouldReturnLoan() throws Exception{	
		when(mLoanService.returnLoan(anyInt()))
			.thenReturn(sampleLoan);
		
		sampleLoan.setDevice(sampleDevice);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/loan/return/" + anyInt()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("loanID").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("deleted").value(false))
			.andExpect(MockMvcResultMatchers.jsonPath("returned").value(false));
		
		verify(mLoanService).returnLoan(anyInt());
		verifyNoMoreInteractions(mLoanService);
	}
	
	@Test 
	public void shouldGetLoan() throws Exception {
		sampleLoanList = new ArrayList<>();
		for(int i = 0; i<5; i++)
			sampleLoanList.add(sampleLoan);
		
		when(mLoanService.getLoan())
			.thenReturn(sampleLoanList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/loan"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].loanID").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].deleted").value(false))
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].returned").value(false));
		
		verify(mLoanService).getLoan();
		verifyNoMoreInteractions(mLoanService);
	}
	
}
