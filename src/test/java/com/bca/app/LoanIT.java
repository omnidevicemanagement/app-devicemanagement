package com.bca.app;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bca.app.controller.LoanController;
import com.bca.app.model.MockBorrower;
import com.bca.app.model.MockDevice;
import com.bca.app.model.MockLoan;
import com.bca.app.repository.LoanRepository;
import com.google.gson.JsonObject;

@SpringBootTest(classes = {MockDevice.class, MockBorrower.class, MockLoan.class})
@RunWith(SpringRunner.class)
public class LoanIT {

	final int SAMPLE_LOAN_BORROWER_ID = 1;
	final int SAMPLE_LOAN_DEVICE_ID = 1;
	final Date SAMPLE_LOAN_FROM_LOAN = new Date(System.currentTimeMillis()-100000);
	final Date SAMPLE_LOAN_TO_LOAN = new Date(System.currentTimeMillis()+100000);
	final String SAMPLE_LOAN_CREATED_BY = "Me";
	final String SAMPLE_LOAN_MODIFIED_BY = "You";
	final String SAMPLE_LOAN_USER_DESCRIPTION = "Nothing do describe";
	
	MockMvc mockMvc;
	
	JsonObject requestBody;
	JsonObject borrowerRequestBody;
	JsonObject deviceRequestBody;
	
	@Autowired
	LoanController mLoanController;
	
	@Autowired
	LoanRepository repository;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(mLoanController).build();
	}
	
	@Test
	public void shouldInsertLoan() throws Exception{
		borrowerRequestBody = new JsonObject();
		borrowerRequestBody.addProperty("idBorrower",SAMPLE_LOAN_BORROWER_ID);
		
		deviceRequestBody = new JsonObject();
		deviceRequestBody.addProperty("idDevice",SAMPLE_LOAN_DEVICE_ID);
	
		requestBody = new JsonObject();
		requestBody.add("borrower", borrowerRequestBody);
		requestBody.add("device", deviceRequestBody);
		requestBody.addProperty("fromLoan", ""+SAMPLE_LOAN_FROM_LOAN);
		requestBody.addProperty("toLoan", ""+SAMPLE_LOAN_TO_LOAN);
		requestBody.addProperty("createdBy", SAMPLE_LOAN_CREATED_BY);
		requestBody.addProperty("modifiedBy", SAMPLE_LOAN_MODIFIED_BY);
		requestBody.addProperty("userDescription", SAMPLE_LOAN_USER_DESCRIPTION);

		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/loan")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("loanID").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("fromLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("toLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("deleted").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("returned").exists())
			.andReturn();
		System.out.println("Insertion response: " + mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void shouldGetCurrentLoanUser() throws Exception{
		
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/loan/current/1"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].loanID").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].toLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].fromLoan").exists())
			.andReturn();
	
		System.out.println("Get current loan user response: " + mvcResult.getResponse().getContentAsString());
	}
	
	@Test
	public void shouldGetHistoryLoanUser() throws Exception{
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/loan/history/1"))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.[0].loanID").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.[0].toLoan").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.[0].fromLoan").exists())
				.andReturn();
		
		System.out.println("Get history loan user response: " + mvcResult.getResponse().getContentAsString());
	}
	
	@Test
	public void shouldReturnLoan() throws Exception{
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/loan/return/1"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("loanID").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("fromLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("toLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("deleted").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("returned").exists())
			.andReturn();
		
		System.out.println("Get return response: " + mvcResult.getResponse().getContentAsString());
	}
	
	@Test
	public void shouldGetLoanList() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/loan"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].loanID").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].fromLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].toLoan").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].deleted").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].returned").exists());

	}
}
