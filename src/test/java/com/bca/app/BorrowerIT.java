package com.bca.app;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bca.app.controller.BorrowerController;
import com.bca.app.model.MockBorrower;
import com.google.gson.JsonObject;

@SpringBootTest(classes = {MockBorrower.class})
@RunWith(SpringRunner.class)
public class BorrowerIT {
	
	MockMvc mockMvc;
	
	JsonObject requestBody;
	
	@Autowired
	BorrowerController mBorrowerController;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(mBorrowerController).build();
	}
	
	@Test
	public void shouldAddBorrower() throws Exception {
		requestBody = new JsonObject();
		requestBody.addProperty("nama", "Roger");
		requestBody.addProperty("tim", "Customer Touch Point");
		requestBody.addProperty("nip", "54321");
		requestBody.addProperty("createdBy", "Thomas");
		requestBody.addProperty("modifiedBy", "Martha");
		
		mockMvc.perform(MockMvcRequestBuilders.post("/borrower")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(requestBody.toString()))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("idBorrower").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("nip").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("tim").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("isDeleted").exists())			;
	}
	
	@Test
	public void shouldListBorrower() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/borrower"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].idBorrower").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nip").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].tim").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.[0].isDeleted").exists());
	}
	
	@Test
	public void shouldFindBorrower() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/borrower/1"))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("idBorrower").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("nip").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("nama").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("tim").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("createdAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("updatedAt").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("isDeleted").exists());
	}
	
	
}
