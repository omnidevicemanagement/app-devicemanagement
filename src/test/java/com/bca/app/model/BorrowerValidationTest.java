package com.bca.app.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class BorrowerValidationTest {

	private static ValidatorFactory mValidatorFactory;
	private static Validator mValidator;
	
	final int SAMPLE_ID_BORROWER = 1;
	final String SAMPLE_NIP = "12345";
	final String SAMPLE_NAMA = "Gerald";
	final String SAMPLE_TIM = "Customer Touch Point";
	final Date SAMPLE_CREATED_AT = new Date();
	final Date SAMPLE_UPDATED_AT = new Date();
	final boolean SAMPLE_IS_DELETED = false;
	final String SAMPLE_CREATED_BY = "Me";
	final String SAMPLE_MODIFIED_BY = "You";
	final String SAMPLE_USER_DESCRIPTION = "Tested on October 2018";

	Borrower validBorrower;
	Set<ConstraintViolation<Borrower>> violations;
	ConstraintViolation<Borrower> violation;
	
	@BeforeClass
	public static void createValidator() {
		mValidatorFactory = Validation.buildDefaultValidatorFactory();
		mValidator = mValidatorFactory.getValidator();
	}
	
	@AfterClass
	public static void close() {
		mValidatorFactory.close();
	}
	
	@Before
	public void init() {
		validBorrower = new Borrower();
		validBorrower.setIdBorrower(SAMPLE_ID_BORROWER);
		validBorrower.setNip(SAMPLE_NIP);
		validBorrower.setNama(SAMPLE_NAMA);
		validBorrower.setTim(SAMPLE_TIM);
		validBorrower.setCreatedAt(SAMPLE_CREATED_AT);
		validBorrower.setUpdatedAt(SAMPLE_UPDATED_AT);
		validBorrower.setIsDeleted(SAMPLE_IS_DELETED);
		validBorrower.setCreatedBy(SAMPLE_CREATED_BY);
		validBorrower.setModifiedBy(SAMPLE_MODIFIED_BY);
		validBorrower.setUserDescription(SAMPLE_USER_DESCRIPTION);
	}
	
	@Test
	public void shouldHaveNoViolation() {
		violations = mValidator.validate(validBorrower);
		assertTrue(violations.isEmpty());
	}
	
	@Test
	public void shouldHaveNipViolation() {
		validBorrower.setNip("1234");
		violations = mValidator.validate(validBorrower);
		
		violation = violations.iterator().next();
		
		assertEquals("NIP should have atleast 5 numbers", violation.getMessage());
		assertEquals("1234", violation.getInvalidValue());
	}
	
	@Test
	public void shouldHaveNamaViolation() {
		validBorrower.setNama("Kent");
		violations = mValidator.validate(validBorrower);
		
		violation = violations.iterator().next();
		
		assertEquals("Name should have atleast 5 characters", violation.getMessage());
		assertEquals("Kent", violation.getInvalidValue());
	}
	
	@Test
	public void shouldHaveTimViolation() {
		validBorrower.setTim(null);
		violations = mValidator.validate(validBorrower);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveCreatedByViolation() {
		validBorrower.setCreatedBy(null);
		violations = mValidator.validate(validBorrower);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveModifiedByViolation() {
		validBorrower.setModifiedBy(null);
		violations = mValidator.validate(validBorrower);
		
		assertTrue(!violations.isEmpty());
	}
}
