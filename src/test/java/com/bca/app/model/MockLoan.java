package com.bca.app.model;

import java.sql.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.bca.app.repository.BorrowerRepository;
import com.bca.app.repository.DeviceRepository;
import com.bca.app.repository.LoanRepository;

@TestConfiguration
public class MockLoan {

	final static int SAMPLE_LOAN_BORROWER_ID = 1;
	final static int SAMPLE_LOAN_DEVICE_ID = 1;
	final static Date SAMPLE_LOAN_FROM_LOAN = new Date(System.currentTimeMillis()-100000);
	final static Date SAMPLE_LOAN_TO_LOAN = new Date(System.currentTimeMillis()+100000);
	final static String SAMPLE_LOAN_CREATED_BY = "Me";
	final static String SAMPLE_LOAN_MODIFIED_BY = "You";
	final static String SAMPLE_LOAN_USER_DESCRIPTION = "Nothing do describe";

	final static String SAMPLE_NAMA = "Maria";
	final static String SAMPLE_TYPE = "Samsung Galaxy A8";
	final static String SAMPLE_SERIAL_NUMBER = "SN6734";
	final static String SAMPLE_OS = "Android";
	final static String SAMPLE_DEVICE_DESCRIPTION = "My First Phone";
	final static String SAMPLE_CREATED_BY = "James";
	final static String SAMPLE_MODIFIED_BY = "Martha";
	final static String SAMPLE_BOOK_STATUS = "Available";
	
	final static String SAMPLE_DEVICE_NAMA = "Maria";
	final static String SAMPLE_TIM = "Customer Touch Point";
	final static String SAMPLE_NIP = "16734";
	
	Loan mockLoan = new Loan();
	Device mockDevice = new Device();
	Borrower mockBorrower = new Borrower();
	
	@Bean
	public CommandLineRunner demo(LoanRepository repository, DeviceRepository deviceRepository, BorrowerRepository borrowerRepository) {
		
		//init device to database
		mockDevice.setNama(SAMPLE_NAMA);
		mockDevice.setType(SAMPLE_TYPE);
		mockDevice.setSerialNumber(SAMPLE_SERIAL_NUMBER);
		mockDevice.setOperatingSystem(SAMPLE_OS);
		mockDevice.setDeviceDescription(SAMPLE_DEVICE_DESCRIPTION);
		mockDevice.setCreatedBy(SAMPLE_CREATED_BY);
		mockDevice.setModifiedBy(SAMPLE_MODIFIED_BY);
		mockDevice.setBookStatus(SAMPLE_BOOK_STATUS);
		deviceRepository.save(mockDevice);
		
		//init borrower to database
		mockBorrower.setNama(SAMPLE_DEVICE_NAMA);
		mockBorrower.setTim(SAMPLE_TIM);
		mockBorrower.setNip(SAMPLE_NIP);
		mockBorrower.setCreatedBy(SAMPLE_CREATED_BY);
		mockBorrower.setModifiedBy(SAMPLE_MODIFIED_BY);
		borrowerRepository.save(mockBorrower);
		
		//init current loan to database
		mockLoan.setFromLoan(SAMPLE_LOAN_FROM_LOAN);
		mockLoan.setToLoan(SAMPLE_LOAN_TO_LOAN);
		mockLoan.setCreatedBy(SAMPLE_LOAN_CREATED_BY);
		mockLoan.setModifiedBy(SAMPLE_LOAN_MODIFIED_BY);
		mockLoan.setUserDescription(SAMPLE_LOAN_USER_DESCRIPTION);		
		mockLoan.setBorrower(borrowerRepository.findById(SAMPLE_LOAN_BORROWER_ID).get());
		mockLoan.setDevice(deviceRepository.findById(SAMPLE_LOAN_DEVICE_ID).get());
		repository.save(mockLoan);
		
		//then init history loan to database
		mockLoan.setIsReturned(true);
		repository.save(mockLoan);	
		
		return (args) -> {
			for (Loan loan : repository.findAll()) {
				System.out.println("Loaded loan: " + loan.getLoanID() + " "  + " " + loan.getUserDescription());
			}
			
			for (Device device : deviceRepository.findAll()) {
				System.out.println("Loaded device : " + device.getIdDevice() + " " + device.getBookStatus());
			}
			
			for (Borrower borrower : borrowerRepository.findAll()) {
				System.out.println("Loaded borrower: " + borrower.getIdBorrower() + " " + borrower.getNama());
			}
		};
	}
}
