package com.bca.app.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class DeviceValidationTest {

	private static ValidatorFactory mValidatorFactory;
	private static Validator mValidator;
	
	final String SAMPLE_NAMA = "Maria";
	final String SAMPLE_TYPE = "Samsung Galaxy A8";
	final String SAMPLE_SERIAL_NUMBER = "SN6734";
	final String SAMPLE_OS = "Android";
	final String SAMPLE_DEVICE_DESCRIPTION = "";
	final String SAMPLE_CREATED_BY = "James";
	final String SAMPLE_MODIFIED_BY = "Martha";
	final String SAMPLE_CREATED_AT= "2018-10-07T12:22:40.000+0000";
	final String SAMPLE_UPDATED_AT = "2018-10-07T12:22:40.000+0000";
	final boolean SAMPLE_IS_DELETED = false;
	final String SAMPLE_USER_DESCRIPTION = "description";

	Device validDevice;
	Set<ConstraintViolation<Device>> violations;
	ConstraintViolation<Device> violation;

	@BeforeClass
	public static void createValidator() {
		mValidatorFactory = Validation.buildDefaultValidatorFactory();
		mValidator = mValidatorFactory.getValidator();
	}
	
	@AfterClass
	public static void close() {
		mValidatorFactory.close();
	}
	
	@Before
	public void init() {
		validDevice = new Device();
		validDevice.setNama(SAMPLE_NAMA);
		validDevice.setType(SAMPLE_TYPE);
		validDevice.setSerialNumber(SAMPLE_SERIAL_NUMBER);
		validDevice.setOperatingSystem(SAMPLE_OS);
		validDevice.setDeviceDescription(SAMPLE_DEVICE_DESCRIPTION);
		validDevice.setCreatedBy(SAMPLE_CREATED_BY);
		validDevice.setModifiedBy(SAMPLE_MODIFIED_BY);
		validDevice.setCreatedAt(new Date());
		validDevice.setUpdatedAt(new Date());
		validDevice.setDeviceDescription(SAMPLE_USER_DESCRIPTION);
	}
	
	@Test
	public void shouldHaveNoViolation() {
		violations = mValidator.validate(validDevice);
		assertTrue(violations.isEmpty());
	}
	
	@Test
	public void shouldHaveNamaViolation() {
		validDevice.setNama("Oppo");
		violations = mValidator.validate(validDevice);
		
		violation = violations.iterator().next();
		
		assertEquals("Name should have atleast 5 characters", violation.getMessage());
		assertEquals("Oppo", violation.getInvalidValue());
	}
	
	@Test
	public void shouldHaveTypeViolation() {
		validDevice.setType(null);
		violations = mValidator.validate(validDevice);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveSerialNumberViolation() {
		validDevice.setSerialNumber(null);
		violations = mValidator.validate(validDevice);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveDeviceDescriptionViolation() {
		validDevice.setDeviceDescription(null);
		violations = mValidator.validate(validDevice);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveCreatedByViolation() {
		validDevice.setCreatedBy(null);
		violations = mValidator.validate(validDevice);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveModifiedByViolation() {
		validDevice.setModifiedBy(null);
		violations = mValidator.validate(validDevice);
		
		assertTrue(!violations.isEmpty());
	}
}
