package com.bca.app.model;

import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LoanValidationTest {

	private static ValidatorFactory mValidatorFactory;
	private static Validator mValidator;

	final Date SAMPLE_LOAN_FROM_LOAN = new Date(System.currentTimeMillis()-100000);
	final Date SAMPLE_LOAN_TO_LOAN = new Date(System.currentTimeMillis()+100000);
	final String SAMPLE_LOAN_CREATED_BY = "Me";
	final String SAMPLE_LOAN_MODIFIED_BY = "You";
	final String SAMPLE_LOAN_USER_DESCRIPTION = "Nothing do describe";
			
	Loan validLoan;
	Set<ConstraintViolation<Loan>> violations;
	ConstraintViolation<Loan> violation;
	
	@BeforeClass
	public static void createValidator() {
		mValidatorFactory = Validation.buildDefaultValidatorFactory();
		mValidator = mValidatorFactory.getValidator();
	}
	
	@AfterClass
	public static void close() {
		mValidatorFactory.close();
	}
	
	@Before
	public void init() {
		validLoan = new Loan();
		validLoan.setFromLoan(SAMPLE_LOAN_FROM_LOAN);
		validLoan.setToLoan(SAMPLE_LOAN_TO_LOAN);
		validLoan.setCreatedBy(SAMPLE_LOAN_CREATED_BY);
		validLoan.setModifiedBy(SAMPLE_LOAN_MODIFIED_BY);
		validLoan.setUserDescription(SAMPLE_LOAN_USER_DESCRIPTION);
	}
	
	@Test
	public void shouldHaveNoViolation() {
		violations = mValidator.validate(validLoan);
		assertTrue(violations.isEmpty());
	}
	
	@Test
	public void shouldHaveFromLoanViolation() {
		validLoan.setFromLoan(null);
		violations = mValidator.validate(validLoan);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveToLoanViolation() {
		validLoan.setToLoan(null);
		violations = mValidator.validate(validLoan);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveCreatedByViolation() {
		validLoan.setCreatedBy(null);
		violations = mValidator.validate(validLoan);
		
		assertTrue(!violations.isEmpty());
	}
	
	@Test
	public void shouldHaveModifiedByViolation() {
		validLoan.setModifiedBy(null);
		violations = mValidator.validate(validLoan);
		
		assertTrue(!violations.isEmpty());
	}
}
