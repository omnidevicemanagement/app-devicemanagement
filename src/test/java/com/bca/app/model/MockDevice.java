package com.bca.app.model;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.bca.app.repository.DeviceRepository;

@TestConfiguration
public class MockDevice {

	final static String SAMPLE_NAMA = "Maria";
	final static String SAMPLE_TYPE = "Samsung Galaxy A8";
	final static String SAMPLE_SERIAL_NUMBER = "SN6734";
	final static String SAMPLE_OS = "Android";
	final static String SAMPLE_DEVICE_DESCRIPTION = "My First Phone";
	final static String SAMPLE_CREATED_BY = "James";
	final static String SAMPLE_MODIFIED_BY = "Martha";
	
	Device mockDevice = new Device();
	
	@Bean
	public CommandLineRunner demo(DeviceRepository repository) {
		
		mockDevice.setNama(SAMPLE_NAMA);
		mockDevice.setType(SAMPLE_TYPE);
		mockDevice.setSerialNumber(SAMPLE_SERIAL_NUMBER);
		mockDevice.setOperatingSystem(SAMPLE_OS);
		mockDevice.setDeviceDescription(SAMPLE_DEVICE_DESCRIPTION);
		mockDevice.setCreatedBy(SAMPLE_CREATED_BY);
		mockDevice.setModifiedBy(SAMPLE_MODIFIED_BY);
		
		return (args) -> {
			repository.save(mockDevice);
	
			for (Device device : repository.findAll()) {
				System.out.println("Loaded device: " + device.getIdDevice());
			}
		};
	}

}
