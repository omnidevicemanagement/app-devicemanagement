package com.bca.app.model;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.bca.app.repository.BorrowerRepository;

@TestConfiguration
public class MockBorrower {

	final static String SAMPLE_NAMA = "Maria";
	final static String SAMPLE_TIM = "Customer Touch Point";
	final static String SAMPLE_NIP = "16734";
	final static String SAMPLE_CREATED_BY = "Jordan";
	final static String SAMPLE_MODIFIED_BY = "Martha";
	
	Borrower mockBorrower = new Borrower();
	
	@Bean
	public CommandLineRunner demo(BorrowerRepository repository) {
		
		mockBorrower.setNama(SAMPLE_NAMA);
		mockBorrower.setTim(SAMPLE_TIM);
		mockBorrower.setNip(SAMPLE_NIP);
		mockBorrower.setCreatedBy(SAMPLE_CREATED_BY);
		mockBorrower.setModifiedBy(SAMPLE_MODIFIED_BY);
		
		return (args) -> {
			repository.save(mockBorrower);
			
			for (Borrower borrower : repository.findAll()) {
				System.out.println("Loaded borrower: " + borrower.getIdBorrower());
			}
		};
	}

}
